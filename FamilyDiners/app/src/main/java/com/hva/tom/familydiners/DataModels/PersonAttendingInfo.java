package com.hva.tom.familydiners.DataModels;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by Tom on 12-10-2017.
 */

public class PersonAttendingInfo implements Serializable {
    private String mName;
    private boolean mIsAttending;
    private long itemID;
    private long mParentListID;

    public PersonAttendingInfo(String name, boolean isAttending, long parentListID) {
        super();
        mName = name;
        mIsAttending = isAttending;
        mParentListID = parentListID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public boolean isAttending() {
        return mIsAttending;
    }

    public void setAttending(boolean attending) {
        mIsAttending = attending;
    }

    public int isAttendingInt()
    {
        return mIsAttending ? 1 : 0;
    }

    public long getParentListID() {
        return mParentListID;
    }

    public void setParentListID(long parentListID) {
        mParentListID = parentListID;
    }

    public long getItemID() {
        return itemID;
    }

    public void setItemID(long itemID) {
        this.itemID = itemID;
    }
}
