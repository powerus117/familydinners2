package com.hva.tom.familydiners;

import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.hva.tom.familydiners.Adapters.DefaultPeopleAdapter;
import com.hva.tom.familydiners.DataModels.DefaultPersonInfo;
import com.hva.tom.familydiners.Dialogs.AddPersonDialogFragment;

public class activity_settings extends AppCompatActivity implements AddPersonDialogFragment.AddPersonDialogListener{

    private DefaultPeopleAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.peopleListView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new DefaultPeopleAdapter();
        recyclerView.setAdapter(mAdapter);

        Button addPersonButton = (Button)findViewById(R.id.addPersonButton);

        addPersonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create new custom dialog and show with a (unused) tag
                DialogFragment newDialog = new AddPersonDialogFragment();
                newDialog.show(getFragmentManager(), "addPersonDialog");
            }
        });
    }

    private void addNewPerson(String personName)
    {
        DefaultPersonInfo aPerson = new DefaultPersonInfo(personName);
        mAdapter.addPerson(aPerson);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String personName) {
        addNewPerson(personName);
    }
}
