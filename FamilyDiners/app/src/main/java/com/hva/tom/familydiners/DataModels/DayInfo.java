package com.hva.tom.familydiners.DataModels;

import java.io.Serializable;

/**
 * Created by Tom on 12-10-2017.
 */

public class DayInfo implements Serializable{

    private String dayName;
    private long mDBItemID;

    public DayInfo(String dayName) {
        this.dayName = dayName;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public long getDBItemID() {
        return mDBItemID;
    }

    public void setDBItemID(long DBItemID) {
        mDBItemID = DBItemID;
    }
}
