package com.hva.tom.familydiners.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Tom on 13-10-2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_FIRST_NAME = "dayInfo";
    public static final String DATABASE_NAME_EXTENSION = ".db";
    public static final String DATABASE_NAME = DATABASE_FIRST_NAME + DATABASE_NAME_EXTENSION;
    public static final int DATABASE_VERSION = 1;

    private static final String DATABASE_DAYS_CREATE =
            "CREATE TABLE " + DaysContract.DayEntry.TABLE_NAME +
                    "(" +
                    DaysContract.DayEntry._ID + " INTEGER PRIMARY KEY, " +
                    DaysContract.DayEntry.COLUMN_NAME_DAY +
                    ");";

    private static final String DATABASE_PERSONS_CREATE =
            "CREATE TABLE " + PersonAttendingContract.PersonAttendingEntry.TABLE_NAME +
                    "(" +
                    PersonAttendingContract.PersonAttendingEntry._ID + " INTEGER PRIMARY KEY, " +
                    PersonAttendingContract.PersonAttendingEntry.PARENT_TABLE_ID + " INTEGER, " +
                    PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_PERSON + " TEXT, " +
                    PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_ATTENDING + " INTEGER" +
                    ");";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_DAYS_CREATE);
        sqLiteDatabase.execSQL(DATABASE_PERSONS_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DaysContract.DayEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PersonAttendingContract.PersonAttendingEntry.TABLE_NAME);

        onCreate(sqLiteDatabase);
    }
}
