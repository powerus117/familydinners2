package com.hva.tom.familydiners.Database;

import android.provider.BaseColumns;

/**
 * Created by Tom on 13-10-2017.
 */

public final class DaysContract {
    private DaysContract(){}

    public static class DayEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "Days";
        public static final String COLUMN_NAME_DAY = "day";
    }
}
