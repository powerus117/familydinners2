package com.hva.tom.familydiners.DataModels;

/**
 * Created by Tom on 5-12-2017.
 */

public class DefaultPersonInfo {

    String mName;

    public DefaultPersonInfo(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
