package com.hva.tom.familydiners.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hva.tom.familydiners.DataModels.DayInfo;
import com.hva.tom.familydiners.R;

import java.util.List;

/**
 * Created by Tom on 9-10-2017.
 */

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.ViewHolder> {

    final private DayClickListener mDayClickListener;

    private List<DayInfo> mDays;

    public DaysAdapter(DayClickListener dayClickListener, List<DayInfo> days) {
        mDayClickListener = dayClickListener;
        mDays = days;
    }

    @Override
    public DaysAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_day_item, null);

        // Return a new holder instance
        DaysAdapter.ViewHolder viewHolder = new DaysAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DaysAdapter.ViewHolder holder, final int position) {
        //Setting viewHolder Data
        final String dayName =  mDays.get(position).getDayName();
        holder.textView.setText(dayName);

        //Setting Clicklistener(s)
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDayClickListener.dayOnClick(dayName, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDays.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textView;
        public View mView;

        public ViewHolder(View itemView)
        {
            super(itemView);
            textView = itemView.findViewById(R.id.dayName);
            mView = itemView;
        }
    }

    public interface DayClickListener
    {
        void dayOnClick(String dayName, int position);
    }
}
