package com.hva.tom.familydiners.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.hva.tom.familydiners.DataModels.PersonAttendingInfo;
import com.hva.tom.familydiners.R;

import java.util.List;

/**
 * Created by Tom on 9-10-2017.
 */

public class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.ViewHolder> {

    final private PersonClickListener mPersonClickListener;

    private List<PersonAttendingInfo> mPersons;

    public PersonsAdapter(PersonClickListener personClickListener, List<PersonAttendingInfo> persons) {
        // Get clicklistener implementation from the activity
        mPersonClickListener = personClickListener;
        mPersons = persons;
    }

    @Override
    public PersonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        // Create a view with the custom layout xml
        View view = LayoutInflater.from(context).inflate(R.layout.row_person_item, null);

        // Return a new holder instance
        PersonsAdapter.ViewHolder viewHolder = new PersonsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PersonsAdapter.ViewHolder holder, final int position) {
        // Getting variables
        final String personName =  mPersons.get(position).getName();
        final boolean isAttending =  mPersons.get(position).isAttending();

        // Setting variables in the layout
        holder.textView.setText(personName);
        holder.checkBox.setChecked(isAttending);

        // Adding clicklisteners

        // The checkbox has a clicklistener so the activity can change things like the database when changed
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) view;
                mPersonClickListener.checkboxOnClick(checkBox.isChecked(), position);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mPersonClickListener.personOnLongClick(position);
                return true;
            }
        });
    }

    public PersonAttendingInfo getItemByPosition(int position)
    {
        return mPersons.get(position);
    }

    public void swapItems(List<PersonAttendingInfo> newItems)
    {
        this.mPersons = newItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textView;
        public CheckBox checkBox;

        public ViewHolder(View itemView)
        {
            super(itemView);
            // Setting xml references
            textView = itemView.findViewById(R.id.personName);
            checkBox = itemView.findViewById(R.id.isAttending);
        }
    }

    // Clicklistener for all click actions on a array item, implementation in activity
    public interface PersonClickListener
    {
        void checkboxOnClick(boolean isChecked, int position);
        void personOnLongClick(int position);
    }
}
