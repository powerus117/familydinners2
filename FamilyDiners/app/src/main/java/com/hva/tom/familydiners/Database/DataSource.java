package com.hva.tom.familydiners.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hva.tom.familydiners.DataModels.DayInfo;
import com.hva.tom.familydiners.DataModels.PersonAttendingInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tom on 13-10-2017.
 */

public class DataSource {
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private DBHelper mDBHelper;
    private String[] DAYS_ALL_COLUMNS = {  DaysContract.DayEntry._ID,
            DaysContract.DayEntry.COLUMN_NAME_DAY};
    private String[] PERSONS_ALL_COLUMNS = { PersonAttendingContract.PersonAttendingEntry._ID,
            PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_PERSON,
            PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_ATTENDING,
            PersonAttendingContract.PersonAttendingEntry.PARENT_TABLE_ID};

    public DataSource(Context context) {
        mContext = context;
        mDBHelper = new DBHelper(mContext);
    }

    // Opens the database to use it
    public void open()  {
        mDatabase = mDBHelper.getWritableDatabase();
    }
    // Closes the database when you no longer need it
    public void close() {
        mDBHelper.close();
    }

    public void createDay(DayInfo dayInfo) {
        ContentValues values = new ContentValues();
        values.put(DaysContract.DayEntry.COLUMN_NAME_DAY, dayInfo.getDayName());
        mDatabase.insert(DaysContract.DayEntry.TABLE_NAME, null, values);
    }

    public void createPerson(PersonAttendingInfo personInfo) {
        ContentValues values = new ContentValues();
        values.put(PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_PERSON, personInfo.getName());
        values.put(PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_ATTENDING, personInfo.isAttendingInt());
        values.put(PersonAttendingContract.PersonAttendingEntry.PARENT_TABLE_ID, personInfo.getParentListID());
        mDatabase.insert(PersonAttendingContract.PersonAttendingEntry.TABLE_NAME, null, values);
    }

    public void updatePersonAttending(long id, PersonAttendingInfo personInfo)
    {
        ContentValues args = new ContentValues();
        args.put(PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_ATTENDING, personInfo.isAttendingInt());

        mDatabase.update(PersonAttendingContract.PersonAttendingEntry.TABLE_NAME, args,
                PersonAttendingContract.PersonAttendingEntry._ID + " = ?",
                new String[]{Long.toString(id)});
    }

    public void deletePerson(long id)
    {
        mDatabase.delete(PersonAttendingContract.PersonAttendingEntry.TABLE_NAME,
                PersonAttendingContract.PersonAttendingEntry._ID + " = ?",
                new String[]{ String.valueOf(id)});
    }

    public List<DayInfo> getAllDays() {
        List<DayInfo> result = new ArrayList<>();

        // Get cursor from database with all days
        Cursor cursor = mDatabase.query(DaysContract.DayEntry.TABLE_NAME,
                DAYS_ALL_COLUMNS, null, null, null, null, null);

        // Read all days from cursor
        while (cursor.moveToNext()) {
            // Get data from current cursor target
            String dayName = getStringByColumName(cursor, DaysContract.DayEntry.COLUMN_NAME_DAY);
            long id = getLongByColumName(cursor, DaysContract.DayEntry._ID);

            // Assign found data to a new object
            DayInfo item = new DayInfo(dayName);
            item.setDBItemID(id);

            result.add(item);
        }
        cursor.close();
        return result;
    }

    public List<PersonAttendingInfo> getAllPeopleOfDay(long dayID) {
        List<PersonAttendingInfo> result = new ArrayList<>();

        // Get cursor from database with all persons where their dayID (parentID) is the same as the requested dayID
        Cursor cursor = mDatabase.query(PersonAttendingContract.PersonAttendingEntry.TABLE_NAME, null,
                PersonAttendingContract.PersonAttendingEntry.PARENT_TABLE_ID + " = ?",
                new String[] { String.valueOf(dayID) }, null, null, null, null);

        while (cursor.moveToNext()) {
            // Get data from current cursor target
            String personName = getStringByColumName(cursor, PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_PERSON);
            boolean isAttending = getIntByColumName(cursor, PersonAttendingContract.PersonAttendingEntry.COLUMN_NAME_ATTENDING) == 1;
            long dayListID = getIntByColumName(cursor, PersonAttendingContract.PersonAttendingEntry.PARENT_TABLE_ID);
            long itemID = getLongByColumName(cursor, PersonAttendingContract.PersonAttendingEntry._ID);

            // Assign found data to new object
            PersonAttendingInfo item = new PersonAttendingInfo(personName, isAttending, dayListID);
            item.setItemID(itemID);

            result.add(item);
        }
        cursor.close();
        return result;
    }

    private int getIntByColumName(Cursor cursor, String tableColumn) {
        return cursor.getInt(cursor.getColumnIndex(tableColumn));
    }

    private long getLongByColumName(Cursor cursor, String tableColumn) {
        return cursor.getLong(cursor.getColumnIndex(tableColumn));
    }

    private String getStringByColumName(Cursor cursor, String tableColumn) {
        return cursor.getString(cursor.getColumnIndex(tableColumn));
    }
}
