package com.hva.tom.familydiners.Database;

import android.provider.BaseColumns;

/**
 * Created by Tom on 13-10-2017.
 */

public final class PersonAttendingContract {
    private PersonAttendingContract(){}

    public static class PersonAttendingEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "Persons";
        public static final String COLUMN_NAME_PERSON = "personName";
        public static final String COLUMN_NAME_ATTENDING = "attending";
        // ID of which day the person belongs to
        public static final String PARENT_TABLE_ID = "parent_id";
    }
}
