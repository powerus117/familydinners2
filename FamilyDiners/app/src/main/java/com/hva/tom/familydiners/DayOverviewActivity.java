package com.hva.tom.familydiners;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.hva.tom.familydiners.Adapters.DaysAdapter;
import com.hva.tom.familydiners.DataModels.DayInfo;
import com.hva.tom.familydiners.Database.DataSource;

import java.util.List;

public class DayOverviewActivity extends AppCompatActivity implements DaysAdapter.DayClickListener {

    public static final String INTENT_DAYID = "dayInfo";
    public static final int REQUESTCODE = 2;
    public static final String PREFS_NAME = "preferenceFile";
    public static final String FIRST_TIME_BOOL_NAME = "FirstTime";

    private RecyclerView mRecyclerView;
    private DaysAdapter mAdapter;

    private List<DayInfo> mDays;
    //private List<PersonAttendingInfo> defaultPeople;

    private DataSource mDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_overview);

        mDataSource = new DataSource(this);
        mDataSource.open();

        // Check if user starts app for first time to app default data to db
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE); //TODO

        if(settings.getBoolean(FIRST_TIME_BOOL_NAME, true))
        {
            Log.d("First time", "First time users starts app, adding default data to db");

            // Filling the list
            mDataSource.createDay(new DayInfo(getString(R.string.monday)));
            mDataSource.createDay(new DayInfo(getString(R.string.tuesday)));
            mDataSource.createDay(new DayInfo(getString(R.string.wednesday)));
            mDataSource.createDay(new DayInfo(getString(R.string.thursday)));
            mDataSource.createDay(new DayInfo(getString(R.string.friday)));
            mDataSource.createDay(new DayInfo(getString(R.string.saturday)));
            mDataSource.createDay(new DayInfo(getString(R.string.sunday)));

            settings.edit().putBoolean(FIRST_TIME_BOOL_NAME, false).apply();
        }

//        //Filling default people list
//        defaultPeople = new ArrayList<>();
//        defaultPeople.add(new PersonAttendingInfo("Dad", true, -1));
//        defaultPeople.add(new PersonAttendingInfo("Mom", true, -1));
//        defaultPeople.add(new PersonAttendingInfo("Jos", false, -1));
//        defaultPeople.add(new PersonAttendingInfo("Tom", true, -1));

        mDays = mDataSource.getAllDays();

        mRecyclerView = (RecyclerView) findViewById(R.id.daysOverview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new DaysAdapter(this, mDays);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void dayOnClick(String dayName, int position) {
        Intent intent = new Intent(DayOverviewActivity.this, AttendanceOverview.class);

        // Add the item ID that is being edited so the right element can be changed in the database
        intent.putExtra(INTENT_DAYID, mDays.get(position).getDBItemID());
        startActivityForResult(intent, REQUESTCODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.day_overview_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.settings:
                GoToSettingsActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void GoToSettingsActivity()
    {
        Intent intent = new Intent(DayOverviewActivity.this, activity_settings.class);

        startActivity(intent);
    }
}
