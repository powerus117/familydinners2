package com.hva.tom.familydiners;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.hva.tom.familydiners.Adapters.PersonsAdapter;
import com.hva.tom.familydiners.DataModels.PersonAttendingInfo;
import com.hva.tom.familydiners.Database.DataSource;
import com.hva.tom.familydiners.Dialogs.AddPersonDialogFragment;

public class AttendanceOverview extends AppCompatActivity implements PersonsAdapter.PersonClickListener, AddPersonDialogFragment.AddPersonDialogListener {

    public static final boolean DEFAULT_ATTENDANCE = true;

    private RecyclerView mRecyclerView;

    private Button mDoneButton;
    private Button mAddPersonButton;

    private long mDayID;

    private DataSource mDataSource;
    private PersonsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_overview);

        mDataSource = new DataSource(this);
        mDataSource.open();

        Intent intent = getIntent();

        mDayID = (Long) intent.getSerializableExtra(DayOverviewActivity.INTENT_DAYID);

        mRecyclerView = (RecyclerView) findViewById(R.id.personOverview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new PersonsAdapter(this, mDataSource.getAllPeopleOfDay(mDayID));
        mRecyclerView.setAdapter(mAdapter);

        // Assign xml variables
        mDoneButton = (Button)findViewById(R.id.saveButton);
        mAddPersonButton = (Button) findViewById(R.id.addPerson);

        // Clicklisteners
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAndQuit();
            }
        });

        mAddPersonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create new custom dialog and show with a (unused) tag
                DialogFragment newDialog = new AddPersonDialogFragment();
                newDialog.show(getFragmentManager(), "addPersonDialog");
            }
        });
    }

    public void updateUI()
    {
        // Retrieve new items from database and refresh adapter
        mAdapter.swapItems(mDataSource.getAllPeopleOfDay(mDayID));
    }

    private void saveAndQuit()
    {
        setResult(Activity.RESULT_OK);
        mDataSource.close();
        finish();
    }

    private void addNewPerson(String personName, boolean isAttending)
    {
        PersonAttendingInfo aPerson = new PersonAttendingInfo(personName, isAttending, mDayID);
        mDataSource.createPerson(aPerson);
        updateUI();
    }

    public void checkboxOnClick(boolean isChecked, int position) {
        // Get the changed person from the adapter
        PersonAttendingInfo clickedPerson = mAdapter.getItemByPosition(position);
        // Change the value(s) with the new value(s)
        clickedPerson.setAttending(isChecked);
        // Replace the item in database with the new changed item
        mDataSource.updatePersonAttending(clickedPerson.getItemID(), clickedPerson);
    }

    @Override
    public void personOnLongClick(int position) {
        mDataSource.deletePerson(mAdapter.getItemByPosition(position).getItemID());
        updateUI();
    }

    // User has added a new person through the dialog
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String personName) {
        addNewPerson(personName, DEFAULT_ATTENDANCE);
    }
}
