package com.hva.tom.familydiners.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hva.tom.familydiners.DataModels.DefaultPersonInfo;
import com.hva.tom.familydiners.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tom on 5-12-2017.
 */

public class DefaultPeopleAdapter extends RecyclerView.Adapter<DefaultPeopleAdapter.ViewHolder>{
    private List<DefaultPersonInfo> mValues;

    public DefaultPeopleAdapter() {
        mValues = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_def_person_item, null);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String personName = mValues.get(position).getName();

        holder.personNameTextView.setText(personName);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView personNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            personNameTextView = itemView.findViewById(R.id.personNameText);
        }
    }

    public void addPerson(DefaultPersonInfo newPerson)
    {
        mValues.add(newPerson);
        notifyDataSetChanged();
    }
}
